//Declarare vectori (matrici unidimensionale)
var titlu = ["Produs1",
                 "Produs2",
                 "Produs3",
                ];
var descriere = ["Nike Air1",
                    "Nike Air2",
                     "Nike Air3"
                    ];
var categorie = ["Sport",
                  "Alergare"
                 ];

var pret = ["500","300","400"]; 
//    ["Leonardo da Vinci",
//                    "Edvard Munch"
//                  ];

var cantitate = ["3","4","5"];

var imagine = [
                 "assets/images/p7.jpg",
                 "assets/images/p8.jpg",
                 "assets/images/p9.jpg"
];



createItemContainer1();
createItemContainer2();
createItemContainer3();

function cumpara(){
   window.alert("Ai cumparat produsul!");
}

function createItemContainer1(){
    var element1 = document.getElementById('element1');
//    console.log(element1);
    
    var titluContainer = document.createElement('p');
    titluContainer.id='titluContainer';
    titluContainer.innerHTML = titlu[0];
    element1.appendChild(titluContainer);
    
    var descriereContainer = document.createElement('p');
    descriereContainer.id='descriereContainer';
    descriereContainer.innerHTML = descriere[0];
    element1.appendChild(descriereContainer);
    
    var categorieContainer = document.createElement('p');
    categorieContainer.id='categorieContainer';
    categorieContainer.innerHTML=categorie[0];
    element1.appendChild(categorieContainer);
    
    var pretContainer = document.createElement('p');
    pretContainer.id='pretContainer';
    pretContainer.innerHTML=pret[0];
    element1.appendChild(pretContainer);
    
    var cantitateContainer = document.createElement('p');
    cantitateContainer.id='cantitateContainer';
    cantitateContainer.innerHTML=cantitate[0];
    element1.appendChild(cantitateContainer);
    
    var imagineContainer = document.createElement('img');
    imagineContainer.id='imagineContainer';
    imagineContainer.src=imagine[0];
    element1.appendChild(imagineContainer);
    
    var button = document.createElement('button');
    button.className="cumpara";
    button.innerHTML = "Cumpara";
    button.style.float="left";
    button.addEventListener("click", function() {
          cumpara(); 
        });
    element1.appendChild(button);
}

function createItemContainer2(){
    var element2 = document.getElementById('element2');
    console.log(element2);
    
    var titluContainer2 = document.createElement('p');
    titluContainer2.id='titluContainer2';
    titluContainer2.innerHTML = titlu[1];
    element2.appendChild(titluContainer2);
    
    var descriereContainer2 = document.createElement('p');
    descriereContainer2.id='descriereContainer2';
    descriereContainer2.innerHTML = descriere[1];
    element2.appendChild(descriereContainer2);
    
    var categorieContainer2 = document.createElement('p');
    categorieContainer2.id='categorieContainer2';
    categorieContainer2.innerHTML=categorie[1];
    element2.appendChild(categorieContainer2);
    
    var pretContainer2 = document.createElement('p');
    pretContainer2.id='pretContainer2';
    pretContainer2.innerHTML=pret[1];
    element2.appendChild(pretContainer2);
    
    var cantitateContainer2 = document.createElement('p');
    cantitateContainer2.id='cantitateContainer2';
    cantitateContainer2.innerHTML=cantitate[1];
    element2.appendChild(cantitateContainer2);
    
    var imagineContainer2 = document.createElement('img');
    imagineContainer2.id='imagineContainer2';
    imagineContainer2.src=imagine[1];
    element2.appendChild(imagineContainer2);
    
      var button = document.createElement('button');
    button.className="cumpara";
    button.innerHTML = "Cumpara";
    button.style.float="left";
    button.addEventListener("click", function() {
          cumpara(); 
        });
    element2.appendChild(button);
}
    
function createItemContainer3(){
    var element3 = document.getElementById('element3');
    console.log(element3);
    
    var titluContainer3 = document.createElement('p');
    titluContainer3.id='titluContainer3';
    titluContainer3.innerHTML = titlu[2];
    element3.appendChild(titluContainer3);
    
    var descriereContainer3 = document.createElement('p');
    descriereContainer3.id='descriereContainer2';
    descriereContainer3.innerHTML = descriere[2];
    element3.appendChild(descriereContainer3);
    
    var categorieContainer3 = document.createElement('p');
    categorieContainer3.id='categorieContainer3';
    categorieContainer3.innerHTML=categorie[1];
    element3.appendChild(categorieContainer3);
    
    var pretContainer3 = document.createElement('p');
    pretContainer3.id='pretContainer2';
    pretContainer3.innerHTML=pret[2];
    element3.appendChild(pretContainer3);
    
    var cantitateContainer3 = document.createElement('p');
    cantitateContainer3.id='cantitateContainer3';
    cantitateContainer3.innerHTML=cantitate[2];
    element3.appendChild(cantitateContainer3);
    
    var imagineContainer3 = document.createElement('img');
    imagineContainer3.id='imagineContainer3';
    imagineContainer3.src=imagine[2];
    element3.appendChild(imagineContainer3);
    
     var button = document.createElement('button');
    button.className="cumpara";
    button.innerHTML = "Cumpara";
    button.style.float="left";
    button.addEventListener("click", function() {
          cumpara(); 
        });
    element3.appendChild(button);
    
}